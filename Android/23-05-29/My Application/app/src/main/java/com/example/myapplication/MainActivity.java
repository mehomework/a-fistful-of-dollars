package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        指定对应xml文件
        setContentView(R.layout.activity_main);
//        根据id获取DOM
        TextView tv = findViewById(R.id.tv);
//        设置文本
        tv.setText("我变了");
//        设置页面跳转
        Button btn = findViewById(R.id.but);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, MainActivity2.class);
                startActivity(intent);
            }
        });
        Log.d( "lange","onCreate: ");
    }
}