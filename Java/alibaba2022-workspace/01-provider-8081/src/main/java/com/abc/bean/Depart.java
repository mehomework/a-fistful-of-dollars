package com.abc.bean;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "fieldHandler"}) // 忽略掉参数中的加载（hibernateLazyInitializer：延时加载）
public class Depart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)  // id 自动递增
    private Integer id;

    private String name;

}
