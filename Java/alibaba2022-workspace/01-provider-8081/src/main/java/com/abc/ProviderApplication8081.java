package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProviderApplication8081 {

    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication8081.class, args);
    }

}
