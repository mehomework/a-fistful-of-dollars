package com.abc.config;

import com.abc.ConsumerApplication8082;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class DepartConfig {
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    };
}
