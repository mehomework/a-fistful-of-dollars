package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerApplication8082 {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication8082.class, args);
    }

}
