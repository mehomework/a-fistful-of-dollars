package com.abc.controller;

import com.abc.bean.Depart;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RequestMapping("/consumer/depart")
@RestController
public class DepartController {
    @Autowired
    private RestTemplate template;

    public static final String SERVICE_PROCIER = "http://localhost:8081/provider/depart";
    @PostMapping("/save")
    public boolean saveHandle(@RequestBody Depart depart) {
        String url = SERVICE_PROCIER + "/save";
        return template.postForObject(url, depart, Boolean.class);
    };

    @DeleteMapping("/del/{id}")
    public void delHandle (@PathVariable("id") int id) {
        String url = SERVICE_PROCIER + "/del/" + id;
        template.delete(url);
    }

    @PutMapping("/update")
    public void updateHandle(@RequestBody Depart depart) {
        String url = SERVICE_PROCIER + "/update";
        template.put(url, depart);
    };

    @GetMapping("/get/{id}")
    public Depart getHandle(@PathVariable("id") int id) {
        String url = SERVICE_PROCIER + "/get/" + id;
        return template.getForObject(url, Depart.class);
    };
    @GetMapping("/list")
    public List<Depart> listHandle() {
        String url = SERVICE_PROCIER + "/list";
        return template.getForObject(url, List.class);
    };
}
