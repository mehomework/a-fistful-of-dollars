package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class Springboot3demoApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot3demoApplication.class, args);
    }

}
