package com.example.app.gateway.filter;

import com.example.app.gateway.utils.AppJwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class AuthorizeFilter implements Ordered, GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //1.获取request和response对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        //2.判断是否是登录
        if (request.getURI().getPath().contains("/login")) {
            // 放行
            return chain.filter(exchange);
        }

        //3.获取token
        String token = request.getHeaders().getFirst("token");

        //4.判断token是否存在
        if (StringUtils.isBlank(token)) {
            // 设置 401状态码
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            // 结束当前请求并将当前返回给前端
            return response.setComplete();
        }

        //5.判断token是否有效
        try {
            Claims claimsBody = AppJwtUtil.getClaimsBody(token);
            // 判断是否过期
            int tokenStatus = AppJwtUtil.verifyToken(claimsBody);
            if (tokenStatus == 1 || tokenStatus == 2) {
                // 设置 401状态码
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                // 结束当前请求并将当前返回给前端
                return response.setComplete();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            // 设置 401状态码
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            // 结束当前请求并将当前返回给前端
            return response.setComplete();
        }

        //6.放行
        return chain.filter(exchange);
    }

    /**
     * 优先级设置，值越小，优先级越高（越先执行）
     * @return
     */
    @Override
    public int getOrder() {
        return 0;
    }
}
