package com.example.model.wemedia.vo;

import com.example.model.wemedia.pojos.SysMenu;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class SysMenuVo extends SysMenu {


    private Map<String, String> meta;

    private List<SysMenuVo> children;
}
