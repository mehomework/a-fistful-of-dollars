package com.example.model.user.dtos;

import lombok.Data;

@Data
public class EnrollDto {
    private String phone;
    private String password;
    private String name;
    private String image;
    private String sex;
}
