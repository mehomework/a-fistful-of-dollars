package com.example.model.wemedia.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("sys_menu")
public class SysMenu implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("name")
    private String name;

    @TableField("path")
    private String path;
    @TableField("isVisible")
    private String isVisible;
    @TableField("isDeleted")
    private String isDeleted;
    @TableField("redirect")
    private String redirect;
    @TableField("component")
    private String component;
    @TableField("title")
    private String title;
    @TableField("icon")
    private String icon;
    @TableField("menuLevel")
    private Integer menuLevel;
    @TableField("parentMenuId")
    private Integer parentMenuId;
    @TableField("Permissions")
    private String Permissions;
    @TableField("createUser")
    private String createUser;
    @TableField("createTime")
    private String createTime;
    @TableField("updateUser")
    private String updateUser;
    @TableField("updateTime")
    private String updateTime;
}
