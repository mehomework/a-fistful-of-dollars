package com.example.model.wemedia.dtos;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class SysMenuDto {
    private Integer id;

    private String name;

    private String path;

    private String isVisible;

    private String isDeleted;

    private String redirect;

    private String component;

    private String title;

    private String icon;

    private Integer menuLevel;

    private Integer parentMenuId;

    private String createUser;

    private String createTime;

    private String updateUser;

    private String updateTime;
}
