package com.example.common.exception;

import com.example.model.common.dtos.ResponseResult;
import com.example.model.common.enums.AppHttpCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice // 控制器增强类
@Slf4j
public class ExceptionCatch {
    /**
     * 处理不可控报错
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseResult exception(Exception e) {
        e.printStackTrace();
//        log.error("catch exception: {}", e.getMessage());
        return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
    };

    @ExceptionHandler(CustomException.class)
    public ResponseResult exception(CustomException e) {
//        log.error("catch exception: {}", e);
        return ResponseResult.errorResult(e.getAppHttpCodeEnum());
    };
}
