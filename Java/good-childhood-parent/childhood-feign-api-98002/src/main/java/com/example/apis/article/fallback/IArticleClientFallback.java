package com.example.apis.article.fallback;

import com.example.apis.article.IArticleClient;
import com.example.model.article.dtos.ArticleDto;
import com.example.model.common.dtos.ResponseResult;
import com.example.model.common.enums.AppHttpCodeEnum;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

// 但是由于所在包不同，扫描不到这个包，所以需要去 childhood-wemedia 模块下创建 com/example/wemedia/config/InitConfig.java 在客户端开启降级处理
@Component
public class IArticleClientFallback implements IArticleClient {
    @Override
    public ResponseResult saveArticle(ArticleDto dto) {
        return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR,"获取数据失败");
    }

    @Override
    public Map testeArticle() {
        Map<Object, Object> aa = new HashMap<>();
        aa.put("name", "jiangjile ");
        return aa;
    }
}
