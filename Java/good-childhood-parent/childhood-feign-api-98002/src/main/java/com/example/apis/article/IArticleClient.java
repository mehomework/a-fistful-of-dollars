package com.example.apis.article;

import com.example.apis.article.fallback.IArticleClientFallback;
import com.example.model.article.dtos.ArticleDto;
import com.example.model.common.dtos.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

// 指定降级实现类
@FeignClient(value = "leadnews-article",fallback = IArticleClientFallback.class)
//@FeignClient("leadnews-article")
public interface IArticleClient {

    @PostMapping("/api/v1/article/save")
    public ResponseResult saveArticle(@RequestBody ArticleDto dto);

    @GetMapping("/api/v1/article/test")
    public Map testeArticle();
}
