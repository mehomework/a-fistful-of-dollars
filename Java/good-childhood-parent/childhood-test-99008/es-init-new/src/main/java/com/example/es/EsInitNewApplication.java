package com.example.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsInitNewApplication {
    public static void main(String[] args) {
        SpringApplication.run(EsInitNewApplication.class, args);
        System.out.println("EsInitNewApplication 启动成功");
    }
}
