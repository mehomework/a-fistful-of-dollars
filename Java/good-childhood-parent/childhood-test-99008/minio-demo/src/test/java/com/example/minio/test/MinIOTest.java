package com.example.minio.test;

import com.example.file.service.FileStorageService;
import com.example.minio.MinIOApplication;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

//@SpringBootTest(classes = MinIOApplication.class)
//@ExtendWith(SpringExtension.class)
public class MinIOTest {
// 测试 通过自定义全局模块上传
//    @Autowired
//    private FileStorageService fileStorageService;
//
//    @Test
//    public void test() throws FileNotFoundException {
//        FileInputStream fileInputStream = new FileInputStream("E:/Users/Sunshine/Desktop/HOME_WORK/a-fistful-of-dollars/Java/list.html");
//
//        String path = fileStorageService.uploadHtmlFile("", "list.html", fileInputStream);
//        System.out.println(path);
//    };

    // 正常德莫
    public static void main(String[] args) {

        FileInputStream fileInputStream = null;
        try {

//            fileInputStream =  new FileInputStream("E:/Users/Sunshine/Desktop/HOME_WORK/a-fistful-of-dollars/Java/list.html");

            //1.创建minio链接客户端                                               // 账号           // 密码                           // 启动的服务地址
//            MinioClient minioClient = MinioClient.builder().credentials("fileadmin", "fileadmin").endpoint("http://192.168.1.5:9000").build();
            MinioClient minioClient = MinioClient.builder().credentials("fileadmin", "fileadmin").endpoint("http://192.168.58.128:9000").build();
            //2.上传
//            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
//                    .object("list.html")//文件名
//                    .contentType("text/html")//文件类型
//                    .bucket("childhood")//桶名词  与minio创建的名词一致
//                    .stream(fileInputStream, fileInputStream.available(), -1) //文件流
//                    .build();
            // 上传带文件夹
            fileInputStream =  new FileInputStream("E:/Users/Sunshine/Desktop/HOME_WORK/a-fistful-of-dollars/Java/good-childhood-parent可能会用到的/plugins/js/axios.min.js");

            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .object("plugins/js/axios.min.js")//文件名
                    .contentType("text/js")//文件类型
                    .bucket("childhood")//桶名词  与minio创建的名词一致
                    .stream(fileInputStream, fileInputStream.available(), -1) //文件流
                    .build();
            minioClient.putObject(putObjectArgs);

//            System.out.println("http://192.168.1.5:62222//childhood/ak47.jpg");
//            System.out.println("http://192.168.1.5:9000/childhood/list.html");
            System.out.println("http://192.168.58.128:9000/childhood/list.html");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

//    @Autowired
//    private FileStorageService fileStorageService;

//    @Test
//    public void testUpdateImgFile() {
//        try {
//            FileInputStream fileInputStream = new FileInputStream("E:\\tmp\\ak47.jpg");
//            String filePath = fileStorageService.uploadImgFile("", "ak47.jpg", fileInputStream);
//            System.out.println(filePath);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//    }

}