package com.example.tess4j;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

public class Application {
    public static void main(String[] args) throws TesseractException {

        //创建实例
        Tesseract tesseract = new Tesseract();
        //设置字体库路径
        tesseract.setDatapath("E:\\Users\\Sunshine\\Desktop\\HOME_WORK\\a-fistful-of-dollars\\Java");
        //设置语言 -->简体中文
        tesseract.setLanguage("chi_sim");


        File file = new File("E:\\Users\\Sunshine\\Desktop\\HOME_WORK\\a-fistful-of-dollars\\Java\\good-childhood-parent可能会用到的\\诗词.jpg");
        //识别图片
        String ocr = tesseract.doOCR(file);
        System.out.println("图片识别成功 : " + ocr);
    }
}
