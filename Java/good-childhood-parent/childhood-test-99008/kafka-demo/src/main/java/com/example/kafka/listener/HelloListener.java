package com.example.kafka.listener;

import com.alibaba.fastjson2.JSON;
import com.example.kafka.pojo.User;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class HelloListener {

//    @KafkaListener(topics = "kafka-topic")
    @KafkaListener(topics = "user-topic")
    public void onMessage(String message){
        if(!StringUtils.isEmpty(message)){
            System.out.println(message);
            User user = JSON.parseObject(message, User.class);
            System.out.println(user);
        }

    }
}
