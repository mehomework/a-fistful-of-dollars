package com.example.wemedia.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.model.common.dtos.ResponseResult;
import com.example.model.common.enums.AppHttpCodeEnum;
import com.example.model.wemedia.dtos.SysMenuDto;
import com.example.model.wemedia.pojos.SysMenu;
import com.example.model.wemedia.pojos.WmUser;
import com.example.model.wemedia.vo.SysMenuVo;
import com.example.wemedia.mapper.SysSettingsMapper;
import com.example.wemedia.service.SysSettingsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@Transactional
public class SysSettingsImpl extends ServiceImpl<SysSettingsMapper, SysMenu> implements SysSettingsService {

//    @Autowired
//    private SysSettingsService sysSettingsService;

    /**
     * 添加路由菜单
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult addMenu(SysMenuDto dto) {

        SysMenu sysMenu = new SysMenu();

        BeanUtils.copyProperties(dto, sysMenu);

        save(sysMenu);

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 修改路由菜单
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult updateMenu(SysMenuDto dto) {

        // 获取参数中的ID
        Integer id = dto.getId();
        // 通过id去数据库查询是否有数据
        SysMenu sysMenu = new SysMenu();

        BeanUtils.copyProperties(dto, sysMenu);
        updateById(sysMenu);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 删除路由菜单
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult deletedMenu(SysMenuDto dto) {

        SysMenu sysMenu = new SysMenu();

        BeanUtils.copyProperties(dto, sysMenu);
        sysMenu.setIsDeleted("0");
        updateById(sysMenu);

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 根据Permissions获取所以路由菜单
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findAllMenu(SysMenuDto dto) {
        return ResponseResult.okResult(getMenuList(dto.getParentMenuId()));
    }

    public List<SysMenuVo> getMenuList (Integer id) {
        //子菜单的子菜单
        List<SysMenuVo>childList =new ArrayList<>();

        List<SysMenu> sysMenus = list(Wrappers.<SysMenu>lambdaQuery().eq(SysMenu::getParentMenuId, id));
        if (sysMenus != null) {
            for (SysMenu sysMenu : sysMenus) {
                SysMenuVo menuVo = new SysMenuVo();
                BeanUtils.copyProperties(sysMenu, menuVo);
                HashMap<String, String> map = new HashMap<>();
                map.put("title", sysMenu.getTitle());
                map.put("icon", sysMenu.getIcon());
                menuVo.setMeta(map);
                menuVo.setChildren(getMenuList(sysMenu.getId()));
                childList.add(menuVo);
            }

        }

        return childList;
    }

    /**
     * 根据id获取指定路由菜单
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findIdMenu(SysMenuDto dto) {
        return null;
    }
}
