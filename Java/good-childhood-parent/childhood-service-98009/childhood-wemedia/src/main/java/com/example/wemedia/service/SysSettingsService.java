package com.example.wemedia.service;

import com.example.model.common.dtos.ResponseResult;
import com.example.model.wemedia.dtos.SysMenuDto;

public interface SysSettingsService {
    /**
     * 添加路由菜单
     * @param dto
     * @return
     */
    public ResponseResult addMenu(SysMenuDto dto);

    /**
     * 修改路由菜单
     * @param dto
     * @return
     */
    public ResponseResult updateMenu(SysMenuDto dto);

    /**
     * 删除路由菜单
     * @param dto
     * @return
     */
    public ResponseResult deletedMenu(SysMenuDto dto);

    /**
     * 根据Permissions获取所以路由菜单
     * @param dto
     * @return
     */
    public ResponseResult findAllMenu(SysMenuDto dto);

    /**
     * 根据id获取指定路由菜单
     * @param dto
     * @return
     */
    public ResponseResult findIdMenu(SysMenuDto dto);
}
