package com.example.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.model.wemedia.pojos.SysMenu;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysSettingsMapper extends BaseMapper<SysMenu> {
}
