package com.example.wemedia.controller.v1;

import com.example.model.common.dtos.ResponseResult;
import com.example.model.wemedia.dtos.SysMenuDto;
import com.example.wemedia.service.SysSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/sys")
public class SysSettingsController {

    @Autowired
    private SysSettingsService sysSettingsService;

    @PostMapping("/menu/addMenu")
    public ResponseResult addMenu(@RequestBody SysMenuDto dto) {
        return sysSettingsService.addMenu(dto);
    }
    @PostMapping("/menu/updateMenu")
    public ResponseResult updateMenu(@RequestBody SysMenuDto dto) {
        return sysSettingsService.updateMenu(dto);
    }

    @PostMapping("/menu/getIdMenu")
    public ResponseResult getIdMenu(@RequestBody SysMenuDto dto) {
        return sysSettingsService.findIdMenu(dto);
    }

    @PostMapping("/menu/getMenuList")
    public ResponseResult getMenuList(@RequestBody SysMenuDto dto) {
        return sysSettingsService.findAllMenu(dto);
    }

    @PostMapping("/menu/deletedMenu")
    public ResponseResult deletedMenu(@RequestBody SysMenuDto dto) {
        return sysSettingsService.deletedMenu(dto);
    }
}
