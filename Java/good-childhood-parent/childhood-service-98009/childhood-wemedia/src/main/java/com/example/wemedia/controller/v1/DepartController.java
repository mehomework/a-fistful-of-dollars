package com.example.wemedia.controller.v1;

import com.alibaba.fastjson2.JSONObject;
import com.example.model.article.dtos.ArticleDto;
import com.example.model.common.dtos.ResponseResult;
import netscape.javascript.JSObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@RequestMapping("/consumer/depart")
@RestController
public class DepartController {
    @Autowired
    private RestTemplate template;

    public static final String SERVICE_PROCIER = "http://localhost:9808/api/v1/article";
    @GetMapping("/test")
    public Map testHandle() {
        String url = SERVICE_PROCIER + "/test";
        return template.getForObject(url, Map.class);
    };


    @PostMapping("/save")
    public ResponseResult saveHandle(@RequestBody ArticleDto dto) {
        String url = SERVICE_PROCIER + "/save";
        return template.postForObject(url, dto, ResponseResult.class);
    };

}
