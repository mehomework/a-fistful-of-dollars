package com.example.wemedia;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.example.wemedia.mapper")
// 使用 feign 远程调用接口一定要在启动类加入下面注解，并添加需要扫描包参数
//@EnableFeignClients(basePackages = "com.example.apis.*")
@EnableAsync  // 开启异步调用
@EnableScheduling // 开启调度任务
public class WemediaApplication {

    public static void main(String[] args) {
        SpringApplication.run(WemediaApplication.class,args);
        System.out.println("WemediaApplication. 启动成功！！");
    }
}
