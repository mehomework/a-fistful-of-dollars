package com.example.wemedia.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
//// 指定扫描包路径
@ComponentScan("com.example.apis.article.fallback")
public class InitConfig {
}
