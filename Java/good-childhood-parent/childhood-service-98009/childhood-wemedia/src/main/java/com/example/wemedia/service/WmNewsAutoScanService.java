package com.example.wemedia.service;

// 可以选择接口名 按 ctrl + shift + T 创建测试类
public interface WmNewsAutoScanService {

    /**
     * 自媒体文章审核
     * @param id  自媒体文章id
     */
    public void autoScanWmNews(Integer id);
}
