package com.example.user.controller.v1;

import com.example.model.common.dtos.ResponseResult;
import com.example.model.user.dtos.EnrollDto;
import com.example.model.user.dtos.LoginDto;
import com.example.user.service.ApUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/login")

@Tag(name="APP端用户登录", description = "APP端用户登录")
public class ApUserLoginController {

    @Autowired
    private ApUserService apUserService;
    // swagger 描述具体接口信息，参数信息
    @Operation(summary = "获取登录用户信息", description = "返回单个对象",
            parameters = {@Parameter(name = "phone", description = "手机号"), @Parameter(name = "password", description = "密码")})
    @ApiResponse(responseCode = "200",description = "登录实体对象")
    @PostMapping("/login_auth")
    public ResponseResult login(@RequestBody LoginDto dto) {
        return apUserService.login(dto);
    };

    @PostMapping("/enroll")
    public ResponseResult enroll(@RequestBody EnrollDto dto) {
        return apUserService.enroll(dto);
    };
//    @GetMapping("/login_auth/{phone}")
//    public ResponseResult login(@PathVariable("phone") String phone) {
//        return apUserService.get(phone);
//    };
}
