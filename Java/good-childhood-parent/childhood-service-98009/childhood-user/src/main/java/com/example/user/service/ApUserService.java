package com.example.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.model.common.dtos.ResponseResult;
import com.example.model.user.dtos.EnrollDto;
import com.example.model.user.dtos.LoginDto;
import com.example.model.user.pojos.ApUser;

public interface ApUserService extends IService<ApUser> {
    /**
     * 登录功能
     * @param dto
     * @return
     */
    public ResponseResult login(LoginDto dto);
    /**
     * 注册功能
     * @param dto
     * @return
     */
    public ResponseResult enroll(EnrollDto dto);
    public ResponseResult get(String phone);
}
