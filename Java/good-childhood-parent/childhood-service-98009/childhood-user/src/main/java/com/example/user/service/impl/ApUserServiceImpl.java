package com.example.user.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.model.common.dtos.ResponseResult;
import com.example.model.common.enums.AppHttpCodeEnum;
import com.example.model.user.dtos.EnrollDto;
import com.example.model.user.dtos.LoginDto;
import com.example.model.user.pojos.ApUser;
import com.example.user.mapper.ApUserMapper;
import com.example.user.service.ApUserService;
import com.example.utils.common.AppJwtUtil;
import io.micrometer.common.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
@Transactional
@Slf4j
public class ApUserServiceImpl extends ServiceImpl<ApUserMapper, ApUser> implements ApUserService {

    @Autowired
    private ApUserMapper apUserMapper;
    /**
     * 登录功能
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult login(LoginDto dto) {
        //1.正常登录 用户名和密码
        if (StringUtils.isNotBlank(dto.getPhone()) && StringUtils.isNotBlank(dto.getPassword())){

            //1.1 根据手机号查询用户信息
            ApUser dbUser = getOne(Wrappers.<ApUser>lambdaQuery().eq(ApUser::getPhone, dto.getPhone()));
            if (dbUser == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "用户信息不存在");
            }

            //1.2 比对密码
            String salt = dbUser.getSalt();
            String password = dto.getPassword();
//            log.info("密码为： " + password);
//            log.error("密码为： {}", password);
            String pswd = DigestUtils.md5DigestAsHex((password + salt).getBytes());
            if(!pswd.equals(dbUser.getPassword())){
                return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
            }

            //1.3 返回数据  jwt  user
            String token = AppJwtUtil.getToken(dbUser.getId().longValue());
            Map<String,Object> map = new HashMap<>();
            map.put("token",token);
            dbUser.setSalt("");
            dbUser.setPassword("");
            map.put("user",dbUser);

            return ResponseResult.okResult(map);
        }else {
            //2.游客登录
            Map<String,Object> map = new HashMap<>();
            map.put("token", AppJwtUtil.getToken(0L));
            return ResponseResult.okResult(map);
        }


    }

    /**
     * 注册功能
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult enroll(EnrollDto dto) {
        // 检查参数
        if (dto == null || dto.getName() == null || dto.getPassword() == null || dto.getPhone() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }



        ApUser user = getOne(Wrappers.<ApUser>lambdaQuery().eq(ApUser::getPhone, dto.getPhone()));
        // 如果查询不为null 则表示已经存在此用户了
        if (user != null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST);
        }

        ApUser apUser = new ApUser();
        // 使用下面方法将值赋值
        BeanUtils.copyProperties(dto, apUser);
        // 设置加盐密钥
        String repSalt = UUID.randomUUID().toString().replace("-", "");
        // MD5 加密然后存储
        String md5Pwd = DigestUtils.md5DigestAsHex((dto.getPassword() + repSalt).getBytes());
        apUser.setSalt(repSalt);
        apUser.setPassword(md5Pwd);
        apUser.setCreatedTime(new Date());
        // 存入数据库
        save(apUser);

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
  public   ResponseResult get(String phone) {

      //1.1 根据手机号查询用户信息
      ApUser dbUser = getOne(Wrappers.<ApUser>lambdaQuery().eq(ApUser::getPhone, phone));
      return ResponseResult.okResult(dbUser);
  }
};
