package com.example.schedule.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.model.schedule.pojos.TaskinfoLogs;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liukun
 */
@Mapper
public interface TaskinfoLogsMapper extends BaseMapper<TaskinfoLogs> {

}