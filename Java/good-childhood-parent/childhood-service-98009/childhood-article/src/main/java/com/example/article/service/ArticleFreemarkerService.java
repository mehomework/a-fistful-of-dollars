package com.example.article.service;

import com.example.model.article.pojos.ApArticle;

public interface ArticleFreemarkerService {

    public void buildArticleToMinIO(ApArticle article, String content);
}
