package com.example.article.feign;

import com.example.apis.article.IArticleClient;
import com.example.article.service.ApArticleService;
import com.example.model.article.dtos.ArticleDto;
import com.example.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ArticleClient implements IArticleClient {

    @Autowired
    private ApArticleService apArticleService;

    @PostMapping("/api/v1/article/save")
    @Override
    public ResponseResult saveArticle(@RequestBody ArticleDto dto) {

        return apArticleService.saveArticle(dto);
    }

    @GetMapping("/api/v1/article/test")
    @Override
    public Map testeArticle() {
        Map<Object, Object> aa = new HashMap<>();
        aa.put("name", "chengggggg");
        return aa;
    }
}
