package com.example.search.service.impl;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.query_dsl.Operator;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.elasticsearch.core.search.InnerHitsResult;
import co.elastic.clients.elasticsearch.indices.ElasticsearchIndicesClient;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.example.model.common.dtos.ResponseResult;
import com.example.model.common.enums.AppHttpCodeEnum;
import com.example.model.search.dtos.UserSearchDto;
import com.example.model.user.pojos.ApUser;
import com.example.search.service.ApUserSearchService;
import com.example.search.service.ArticleSearchService;
import com.example.utils.thread.AppThreadLocalUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
//import org.elasticsearch.action.search.SearchRequest;
//import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
//import org.elasticsearch.common.text.Text;
//import org.elasticsearch.index.query.*;
//import org.elasticsearch.search.SearchHit;
//import org.elasticsearch.search.builder.SearchSourceBuilder;
//import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
//import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class ArticleSearchServiceImpl implements ArticleSearchService {

    @Autowired
    private ElasticsearchClient elasticsearchClient;

    @Autowired
    private ApUserSearchService apUserSearchService;

    /**
     * es文章分页检索
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult search(UserSearchDto dto) throws IOException {

        //1.检查参数
        if(dto == null || StringUtils.isBlank(dto.getSearchWords())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        ApUser user = AppThreadLocalUtil.getUser();

        //异步调用 保存搜索记录
        if(user != null && dto.getFromIndex() == 0){
            apUserSearchService.insert(dto.getSearchWords(), user.getId());
        }


        //2.设置查询条件
        //  SearchRequest searchRequest = new SearchRequest("app_info_article");
        //  SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        //获取索引客户端对象
        ElasticsearchIndicesClient indices = elasticsearchClient.indices();
        //创建索引 采用构建器的方式构建(在创建之前需要先判断该索引是否存在)
        boolean exists = indices.exists(u -> u.index("app_info_article")).value();

        //布尔查询
//        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

        //关键字的分词之后查询
        List<Hit<Object>> hits = elasticsearchClient.search(s -> {
            s.query(q -> q.match(
                    m -> m.field("title").field("content")
                            .operator(Operator.Or)
                            .query(dto.getSearchWords())
            ));
            return s;
        }, Object.class).hits().hits();
        System.out.println(hits);
//        QueryStringQueryBuilder queryStringQueryBuilder = QueryBuilders.queryStringQuery(dto.getSearchWords()).field("title").field("content").defaultOperator(Operator.OR);
//        boolQueryBuilder.must(queryStringQueryBuilder);

        //查询小于mindate的数据
//        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("publishTime").lt(dto.getMinBehotTime().getTime());
//        boolQueryBuilder.filter(rangeQueryBuilder);

        //分页查询
//        searchSourceBuilder.from(0);
//        searchSourceBuilder.size(dto.getPageSize());

        //按照发布时间倒序查询
//        searchSourceBuilder.sort("publishTime", SortOrder.DESC);

        //设置高亮  title
//        HighlightBuilder highlightBuilder = new HighlightBuilder();
//        highlightBuilder.field("title");
//        highlightBuilder.preTags("<font style='color: red; font-size: inherit;'>");
//        highlightBuilder.postTags("</font>");
//        searchSourceBuilder.highlighter(highlightBuilder);


//        searchSourceBuilder.query(boolQueryBuilder);
//        searchRequest.source(searchSourceBuilder);
//        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);


        //3.结果封装返回

        List<Map> list = new ArrayList<>();

//        SearchHit[] hits = searchResponse.getHits().getHits();
//        for (Hit<Object> hit : hits) {
//            list.add((Map) hit);
//        }
//        for (SearchHit hit : hits) {
//            String json = hit.getSourceAsString();
//            Map map = JSON.parseObject(json, Map.class);
            //处理高亮
//            if(hit.getHighlightFields() != null && hit.getHighlightFields().size() > 0){
//                Text[] titles = hit.getHighlightFields().get("title").getFragments();
//                String title = StringUtils.join(titles);
                //高亮标题
//                map.put("h_title",title);
//            }else {
                //原始标题
//                map.put("h_title",map.get("title"));
//            }
//            list.add(map);
//        }

        return ResponseResult.okResult(hits);

    }
}
