package com.example.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
public class EsInitApplication {
    public static void main(String[] args) {
        SpringApplication.run(EsInitApplication.class, args);
        System.out.println("EsInitApplication 启动成功！");
    }
}
