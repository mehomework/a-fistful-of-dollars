package com.example.es;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.Result;
import co.elastic.clients.elasticsearch.core.DeleteResponse;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.elasticsearch.indices.CreateIndexResponse;
import co.elastic.clients.elasticsearch.indices.DeleteIndexResponse;
import co.elastic.clients.elasticsearch.indices.ElasticsearchIndicesClient;
import co.elastic.clients.elasticsearch.indices.GetIndexResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.List;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class ApArticleTest {

    @Autowired
    private ElasticsearchClient elasticsearchClient;

    @Test
    public void init() throws Exception {

        //1.查询所有符合条件的文章数据//批量创建文档
//        List<User> users = new ArrayList<>(); //假设有数据

        //获取索引客户端对象
        ElasticsearchIndicesClient indices = elasticsearchClient.indices();

        //2.批量导入到es索引库
//        elasticsearchClient.bulk(b -> {   //批量创建操作
//            users.forEach(u -> {   //遍历需要创建的数据
//                b.operations(
//                        o ->o.create(c -> c.index("app_info_article").id(u.getId().toString()).document(u))
//                );
//            });
//            return b;
//        });

    }

    @Test
    public void getEsInfo () throws IOException {

        //获取索引客户端对象
        ElasticsearchIndicesClient indices = elasticsearchClient.indices();

        //创建索引 采用构建器的方式构建(在创建之前需要先判断该索引是否存在)
        boolean exists = indices.exists(u -> u.index("app_info_article")).value();

        if (exists) {
            System.out.println("该索引已存在！！");
        } else {
            CreateIndexResponse createIndexResponse = indices.create(c -> c.index("app_info_article"));
            System.out.println("创建了索引" + createIndexResponse);
        }

        //查询索引
        GetIndexResponse getResponse = indices.get(g -> g.index("app_info_article"));
        System.out.println("查询索引："+getResponse);

        //删除索引
        DeleteIndexResponse deleteResponse = indices.delete(d -> d.index("app_info_article"));
        System.out.println("删除索引："+deleteResponse.acknowledged());

    }

    @Test
    public void initEsOne() throws IOException {
//        SearchArticleVo searchArticleVo = new SearchArticleVo();
//        searchArticleVo.setId(1302977558807060482L);
//        searchArticleVo.setTitle("武汉高校开学典礼万人歌唱祖国");
//        searchArticleVo.setContent("人合力托举悬窗女童10多名陌生人合力托举悬窗女童10多名陌生人合力托举悬窗女童10多名陌生人合力托举悬窗女童10多名陌生人合力托举悬窗女童10多名陌生人合力托举悬窗女童10多名陌生人合力托举悬窗女童10多名陌");
//        Result result = elasticsearchClient.create(c -> c.index("user1").id(searchArticleVo.getId().toString()).document(searchArticleVo)).result();
//        System.out.println("创建文档："+result);

        //删除文档
//        elasticsearchClient.delete(d -> d.index("user").id(searchArticleVo.getId().toString()));

        //查询文档
        HitsMetadata<Object> hits = elasticsearchClient.search(s -> {
            s.query(q -> q.match(
                    m -> m.field("title").query("武汉")
            ));
            return s;
        }, Object.class).hits();

        System.out.println(hits);
    }
}
