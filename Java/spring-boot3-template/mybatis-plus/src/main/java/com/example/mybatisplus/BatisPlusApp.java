package com.example.mybatisplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatisPlusApp {
    public static void main(String[] args) {
        SpringApplication.run(BatisPlusApp.class, args);
        System.out.println( "BatisPlusApp 启动成功!" );
    }
}
