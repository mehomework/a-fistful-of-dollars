package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;


@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
// 使用feign远程调用接口一定要在启动类加入下面注解，并添加需要扫描包参数
@EnableFeignClients(basePackages = "com.example.apis")
public class Consumer
{
    public static void main( String[] args )
    {
        SpringApplication.run(Consumer.class);
        System.out.println( "Consumer 启动成功!" );
    }
}
