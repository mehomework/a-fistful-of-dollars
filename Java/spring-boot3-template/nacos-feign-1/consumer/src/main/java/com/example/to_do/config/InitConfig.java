package com.example.to_do.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
// 指定扫描包路径
@ComponentScan("com.example.apis.thing.fallback")
public class InitConfig {
}
