package com.example.do_thing.service;

import com.example.do_thing.bean.Depart;

import java.util.List;

public interface DepartService {
    List<Depart> listAllDeparts();
}
