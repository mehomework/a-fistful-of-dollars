package com.example.apis.thing.fallback;

import com.example.apis.thing.IDoClient;
import com.example.do_thing.bean.Depart;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class IDoClientFallback implements IDoClient {
    @Override
    public List<Depart> listHandle() {

        List<Depart> objects = new ArrayList<>();

        Depart depart = new Depart();
        depart.setName("请求返回错误，降级处理");
        objects.add(depart);
        return objects;
    }
}
