package com.example.apis.thing;

import com.example.apis.thing.fallback.IDoClientFallback;
import com.example.do_thing.bean.Depart;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

//@RequestMapping("provider/depart")
//@FeignClient("test")
@FeignClient(value = "test",fallback = IDoClientFallback.class)
public interface IDoClient {
    @GetMapping("provider/depart/list")
    public List<Depart> listHandle();
}
