# childhood-a-gateway-base

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### 当前项目node版本
```
v18.12.1
```

