
import { Tree } from 'ant-design-vue';
// export const TreeNodeItem = {
//     template: `
//       <a-tree-node :key="TreeItem.id" :title="TreeItem.meta.title">
//         <template v-if="TreeItem.children && TreeItem.children.length > 0">
//           <TreeNodeItem v-for="item in TreeItem.children" :key="item.id" :tree-item="item"></TreeNodeItem>
//         </template>
//       </a-tree-node>
//     `,
//     name: 'TreeNodeItem',
//     // must add isSubMenu: true 此项必须被定义
//     isTreeNode: true,
//     props: {
//         // 解构a-sub-menu的属性，也就是文章开头提到的为什么使用函数式组件
//         ...Tree.TreeNode,
//         // Cannot overlap with properties within Menu.SubMenu.props
//         TreeItem: {
//             type: Object,
//             default: () => ({}),
//         },
//     },
//     methods: {
//     },
// };
export default {
    props: {
        treeData: {
            type: Array,
            default() {
                return [
                    // {
                    //     "id": 9,
                    //     "meta": {
                    //         "title": "系统管理",
                    //         "icon": "gitlab"
                    //     },
                    //     "children": [
                    //         {
                    //             "id": 10,
                    //             "meta": {
                    //                 "title": "菜单路由",
                    //                 "icon": ""
                    //             }
                    //         }
                    //     ]
                    // }
                ];
            }
        },
        showLine: {
            type: Boolean,
            default: false,
        },
        showIcon: {
            type: Boolean,
            default: false,
        },
        checkable: {
            type: Boolean,
            default: false,
        }
    },
    methods: {

        onSelect(selectedKeys, info) {
            this.$emit("select", selectedKeys, info)
            // console.log('selected', selectedKeys, info);
        },
        onCheck(checkedKeys, info) {
            this.$emit("check", checkedKeys, info)
            // console.log('onCheck', checkedKeys, info);
        },
    },
    render() {
        function DeepDom(list) {
            let arr = [];
            if (list.length > 0) {
                arr = list.map(t => {
                    return (
                        <a-tree-node key={t.id}>
                            <a-icon slot="icon" type={t.meta.icon || 'smile-o'} />
                            <span slot="title">{t.meta.title}</span>
                            {t.children && DeepDom(t.children)}
                        </a-tree-node>
                    );
                });
            }
            return arr;
        }
        let {treeData, showLine, showIcon, checkable} = this;
        return (
            <a-tree class="u-ant-tree" className="u-ant-tree" show-line={showLine} default-expand-all show-icon={showIcon} checkable={checkable} onSelect={this.onSelect} onCheck={this.onCheck}
            >
                {DeepDom(treeData)}
            </a-tree>
        );
    }
};