
import { Menu } from 'ant-design-vue';
export const MenuItem = {
  template: `
      <a-menu-item @click="routerTo(wrapRouterLink(menuItem))" v-if="!menuItem.children || menuItem.children.length <= 0" :key="menuItem.path" v-bind="$props" v-on="$listeners">
        <a-icon v-if="menuItem.meta.icon" :type="menuItem.meta.icon" />
        <span>{{ menuItem.meta.title }}</span>
      </a-menu-item>
      <a-sub-menu v-else :key="menuItem.path" v-bind="$props" v-on="$listeners">
        <span slot="title">
          <a-icon v-if="menuItem.meta.icon" :type="menuItem.meta.icon" />
          <span>{{ menuItem.meta.title }}</span>
        </span>
        <template v-for="item in menuItem.children">
          <menu-item :key="item.path" :menu-item="item" />
        </template>
      </a-sub-menu>
    `,
  name: 'MenuItem',
  // must add isSubMenu: true 此项必须被定义
  isSubMenu: true,
  props: {
    // 解构a-sub-menu的属性，也就是文章开头提到的为什么使用函数式组件
    ...Menu.SubMenu.props,
    // Cannot overlap with properties within Menu.SubMenu.props
    menuItem: {
      type: Object,
      default: () => ({}),
    },
  },
  methods: {
    wrapRouterLink(item) {
      const route = {
        params: {
          jumpFlag: true
        }
      };

      // if (item.name) {
      //   route.name = item.name;
      // } else
        if (item.path) {
        route.path = this.wrapRouterPath(item.path)
      } else {
        route.path = '/404'
      }
      return route;
    },
    wrapRouterPath(path) {
      if (!path) return "";
      if (path.charAt(0) !== "/") return `/${path}`;
      return path;
    },
    routerTo(path) {
      this.$router.push(path)
    }
  },
};