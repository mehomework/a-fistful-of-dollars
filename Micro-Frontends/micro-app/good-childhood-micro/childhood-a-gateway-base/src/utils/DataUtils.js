const DataUtils = {
  Array: {
      /**
       * 对象数组去重方法
       * @param arr 需要去重的数据
       * @param identifying 去重判断的标识
       * @returns {*[]}
       */
      deduplicationObj(arr, identifying) {
          if (!arr || !Array.isArray(arr)) return arr;
          return Array.from(new Set(arr.map(item => item[identifying]))).map(name => {
              return arr.find(item => item[identifying] === name)
          })
      },
      /**
       * 字符串数组去重
       * @param arr
       * @returns {any[]}
       */
      deduplicationStr(arr) {
          if (!arr || !Array.isArray(arr)) return arr;
          return [...new Set(arr)];
      }
  }
}
export default DataUtils;