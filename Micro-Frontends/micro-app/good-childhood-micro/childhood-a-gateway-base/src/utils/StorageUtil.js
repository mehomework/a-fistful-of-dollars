export const StorageUtil = {
    getItem(key) {
        if (!key) return null;
        const val = localStorage.getItem(key)
        try {
            return JSON.parse(val);
        } catch (e) {
            console.error(e);
            return val;
        }
    },
    setItem(key, val) {
        localStorage.setItem(key, JSON.stringify(val))
    },
    remove(key) {
        localStorage.removeItem(key)
    },
    removeList(keyArr = []) {
        keyArr.forEach(e=>{
            localStorage.removeItem(e)
        })
    },

    clear() {
        localStorage.clear()
    }
}
