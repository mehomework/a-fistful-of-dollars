import post from "@/apis/index"
import { message } from "ant-design-vue"
import axios from "axios";
export const getAPIs = async (that, params, messages, main, minor) => {
    try {
        that.loading = true;
        const resData = await post[main][minor](params);
        if (resData.status != 200) return message.success(resData.statusText);

        // const config = resData.config;

        // console.log(JSON.stringify(resData.data) );
        // if (config.url.indexOf('/enterMeeting/enter') > -1) {
        //     that.$store.commit("UPDATE_GLOBAL", {key: "sessionId", val: resData.headers.sessionid || resData.headers.sessionId || ""})
        // }
        const code = resData && resData.data && resData.data.code;
        if (code === 0 || code === 200) {
            return resData.data
        } else if (code === -1) {
            message.error(resData.data.msg || '接口请求失败');
        } else if (code === 500) {
            message.error(resData.data.msg || resData.data.error || '接口请求失败');
        }

        return false

    } catch (e) {
        console.error(e)
        return false
    } finally {
        that.loading = false;
    }
}

/**
 * 上传文件
 * @param that vue 实例
 * @param files input活动到的文件数据
 * @param urls 后端上传地址
 * @param inputId 获取文件的input ID
 * @param fileTypes 校验只能上传的文件格式
 * @param method 请求方式
 * @returns {Promise<void>}
 */
export const uploadFile = async (that, files, urls, inputId, fileTypes=["pdf"], method="POST") => {
  if (!files || !files.name) {
      throw "请选择需要导入的文件！";
  }

  try {
      let filesNameList = files.name.split(".");
      // 检查当文件格式是否为设定格式
      if (fileTypes.indexOf(filesNameList[filesNameList.length-1]) == -1) {
          throw "上传文件格式错误！";
      }
      const formDdata = new FormData;
      formDdata.append("file", files);
      // 往后端发送上传请求
      const res = await axios({url: urls, method: method, responseType: "blob", params: formDdata, data: formDdata})
      if (res.status != 200) {
          throw "上传失败！";
      }
      // 到此实际上次已经结束，后面为自定义返回数据结构，每个项目可能不一样，视情况而定，可以删除
      const resBlob = await new Response(res.data).json();
      if (!resBlob.success) {
          throw resBlob.msg || "解析数据失败！";
      }

      console.log("上传成功！");
      // 清空上传按钮的数据
      document.getElementById(inputId).value = "";

  } catch (e) {
      console.error(e)
  }
}

/**
 * 下载文件
 * @param urls 后端下载地址
 * @param data 请求参数
 * @param type 下载的文件格式，手打上传也可从请求头获取
 * @param fileName 保存文件名称
 * @param method 请求方式
 * @returns {Promise<void>}
 */
export const downLoad = async (urls, data, fileName, type="xlsx", method="post") => {
    try {
        if (!urls) {
            throw "请求url为空！";
        }
        if (typeof data != "string") {
            data = JSON.stringify(data)
        }
        const res = await axios({url: urls, method: method, responseType: "blob", params: data, data: data, headers: { "content-type": "application/json;charset=UTF-8"}})
        if (res.status != 200) {
            throw "下载失败！";
        }

        const blob = new Blob([res.data]);
        // let name = `${fileName || '未命名'} _ ${new Date().getTime()}.${res.type || type}`
        //获取heads中的filename文件名
        let temp = res.headers["content-disposition"]?res.headers["content-disposition"]:res.headers["Content-Disposition"];
        let name = temp.split('=')
        // //对文件名乱码转义--【Node.js】使用iconv-lite解决中文乱码
        name = decodeURI(name[1]);
        // 通过判断是否IE， IE不支持a标签下载
        if (window.navigator.msSaveBlob) {
            window.navigator.msSaveBlob(blob, name);//兼容IE
        } else {
            // 创建 a 标签进行下载
            let objectUrl = window.URL.createObjectURL(blob);
            const a = document.createElement("a");
            a.href = objectUrl;
            a.style.display = "none";
            a.download = name;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);

            //释放这个临时的对象url
            window.URL.revokeObjectURL(objectUrl);
        }
    } catch (e) {
        console.error(e)
    }
}