export class PostUrl { constructor(name, url, type) { return { name, url, type } } }
export class PostModule { constructor(moduleName, baseUrl, urls) { return { moduleName, baseUrl, urls } } }

// 设置公共路径,  /api/v1 往后端发送请求， /childhood/gateway/locapi/v1 请求本地 json数据
// const basePath = '/api/v1'
const basePath = '/childhood/gateway/locapi/v1'

const postModules = [
    new PostModule('login', '', [
        // 登录
        new PostUrl('loginIn', `${basePath}/login/in`, 'POST'),
    ]),
    // 管理接口
    new PostModule('sys', '', [
        // 获取菜单列表
        new PostUrl('getMenuList', `${basePath}/sys/menu/getMenuList`, 'POST'),
    ]),
]
export default postModules;