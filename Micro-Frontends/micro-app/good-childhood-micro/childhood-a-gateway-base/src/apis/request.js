import axios from 'axios'
import {isNull, eachProp} from '@/utils/Common'
import store from '@/store'
import {StorageUtil} from "@/utils/StorageUtil";

const service = axios.create({
    baseURL: '/',
    timeout: 30000
})

const header = {
    "content-type": "application/json;charset=UTF-8",
};

// request 拦截器
service.interceptors.request.use(originConfig => {
    const config = originConfig;

    // 判断如果走的本地则手动拼接请求json
    if ((config.url.indexOf("/childhood/gateway/locapi/") > -1)) {
        config.method = "GET";
        config.url = config.url + ".json"
    }
    // 非登录接口默认添加token
    if (config.url.indexOf("/login/in") == -1) {
        config.headers.token = StorageUtil.getItem("TOKEN")
    }
    const method = String(config.method).toUpperCase();
    // console.log(config)
    if(method === 'POST'){

    } else if (method === 'GET') {
        config.url = wrapParamUrl(config.url, config.data)
    }
    return config
}, err => Promise.reject(err))

// response 拦截器
service.interceptors.response.use(response => {
    // console.log(response);


    return response;
}, err => Promise.reject(err))

// get 请求,将data 包装到url上
function wrapParamUrl(originUrl, data){
    let url = `${originUrl}?`;
    if(data) {
        eachProp(data, prop => {
            if(!isNull(data[prop])) {
                url += `${prop}=${data[prop].toString()}&`
            }
        })
    }
    // S删除url中最后一个&
    url = url.substring(0, url.length - 1)
    return url;
}

export default service;