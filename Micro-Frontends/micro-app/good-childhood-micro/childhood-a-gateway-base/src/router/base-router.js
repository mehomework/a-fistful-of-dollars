import Layout from '../views/layout/Layout.vue'
import Login from '../views/login/Login.vue'
import { MicroRouterChild } from "./micro-router"

const BaseRouter = [
    {
        path: '/',
        name: '/',
        redirect: '/login'
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    // {
    //     path: '/layout',
    //     name: 'Layout',
    //     component: Layout,
    //     children: [
    //         ...MicroRouterChild,
    //         {
    //             path: '/layout/home',
    //             name: 'Home',
    //             component: () => import(/* webpackChunkName: "Layout" */ '../views/home/Home.vue'),
    //         },
    //     ]
    // }
]

export default BaseRouter;