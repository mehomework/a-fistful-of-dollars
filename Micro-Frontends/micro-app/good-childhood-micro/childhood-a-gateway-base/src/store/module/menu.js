import { StorageUtil } from "@/utils/StorageUtil"
import {filterAsyncRouter} from "@/utils/routeHook";
import DataUtils from "@/utils/DataUtils";

const route = {
    state: {
        // 导航栏路由数据完整数据
        sidBar: StorageUtil.getItem('sidBar') || [],
        // 侧面导航栏数据
        leftsidBar: StorageUtil.getItem('leftsidBar') || [],
        // 当前激活菜单路径
        activeMenu: StorageUtil.getItem('activeMenu') || "",
        // 动态添加的路由数据
        accessedRoutes: [],
        openRoutes: StorageUtil.getItem('openRoutes') || [{
            fullPath: "/layout/home",
            meta: {
                "icon": "home",
                "title": "首页"
            },
            name: "Home",
            path: "/layout/home"
        }],
        openRouteNames: [],
    },
    mutations: {
        UPDATE_OPEN_ROUTES(state, openRoute) {
            if (!state.openRoutes.find(e=>e.path == openRoute.path)) {
                let data = {
                    fullPath: openRoute.fullPath,
                    meta: openRoute.meta,
                    name: openRoute.name,
                    path: openRoute.path
                }
                state.openRoutes.push(data)
                StorageUtil.setItem( 'openRoutes', state.openRoutes )
            }
            if (openRoute.name !== 'Home' && openRoute.name !== 'Login') {

                state.openRouteNames.push(openRoute.name);
                state.openRouteNames = DataUtils.Array.deduplicationStr(state.openRouteNames);
            }

            // state.openRoutes.push(openRoute)
            // state.openRoutes = [...new Set(state.openRoutes)];
            // state.openRoutes = DataUtils.Array.deduplicationStr(state.openRoutes);
            // state.openRoutes = DataUtils.Array.deduplicationObj(state.openRoutes, 'path');
        },
        UPDATE_SIDBAR(state, sidBarArr) {
            state.sidBar = sidBarArr;
            // leftBar 处理数据同步在
            StorageUtil.setItem( 'sidBar', sidBarArr )

            // let newSidBar = JSON.parse(JSON.stringify(sidBarArr));
            //
            // for (let i = 0, len = newSidBar.length; i <len; i++) {
            //     // console.log(i, "i")
            //     const e = newSidBar[i]
            //     if (e.component == "Layout" && e.children) {
            //         for (let j = 0, len = e.children.length; j < len; j++) {
            //             // console.log(j, "j")
            //             // debugger
            //             const m = e.children[j];
            //             if (m.component == "home/Home.vue") {
            //                 newSidBar.unshift(...e.children.splice(j, 1))
            //                 break;
            //             }
            //         }
            //         break;
            //     }
            // }
            // state.leftsidBar = newSidBar;
            // // leftBar 处理数据同步在
            // StorageUtil.setItem( 'leftsidBar', newSidBar )
        },
        UPDATE_LEFT_SIDBAR(state, leftsidBar) {
            state.leftsidBar = leftsidBar;
            StorageUtil.setItem( 'leftsidBar', leftsidBar )
        },
        UPDATE_ACTIVE_MEUN(state, activeMenu) {
            state.activeMenu = activeMenu;
            StorageUtil.setItem( 'activeMenu', activeMenu )
        },
        UPDATE_ACCESSED_ROUTES(state, accessedRoutes) {
            state.accessedRoutes = accessedRoutes;
        }
    },
    actions: {
        UpdataSidBar({ commit, state }, sidBar) {
            // 保存完整数据
            commit("UPDATE_SIDBAR", sidBar);
            // 处理展示在右边真实需要的页面， 如果不做此操作也行，但是首页无法在最上级
            let newSidBar = JSON.parse(JSON.stringify(sidBar));

            for (let i = 0, len = newSidBar.length; i <len; i++) {
                // console.log(i, "i")
                const e = newSidBar[i]
                if (e.name == "LayoutHome" && e.children) {
                   let newChild = e.children
                    newSidBar.splice(i, 1)
                    newSidBar=[...newChild, ...newSidBar]
                   // let newChild = newSidBar.splice(i, 1);
                   //  for (let j = 0, len = newChild.children.length; j < len; j++) {
                   //      // console.log(j, "j")
                   //      // debugger
                   //      const m = newChild.children[j];
                   //      // if (m.component == "home/Home.vue") {
                   //          newSidBar.unshift(m)
                   //          // break;
                   //      // }
                   //  }
                    break;
                    // for (let j = 0, len = e.children.length; j < len; j++) {
                    //     // console.log(j, "j")
                    //     // debugger
                    //     const m = e.children[j];
                    //     if (m.component == "home/Home.vue") {
                    //         newSidBar.unshift(...e.children.splice(j, 1))
                    //         break;
                    //     }
                    // }
                    // break;
                }
            }
            commit("UPDATE_LEFT_SIDBAR", newSidBar);
        },
        // 生成路由
        GenerateRoutes({ commit, state }) {
            return new Promise(resolve => {
            //     // 向后端请求路由数据
            //     getRouters().then(res => {
            //         // 将后台返回的JSON数据转为 路由对象
            //         const accessedRoutes = filterAsyncRouter(res.data)
            //         accessedRoutes.push({ path: '*', redirect: '/404', hidden: true })
            //         commit('SET_ROUTES', accessedRoutes)
            //         resolve(accessedRoutes)
            //     })

               // 将后台返回的JSON数据转为 路由对象
               const accessedRoutes = filterAsyncRouter(state.sidBar)
               accessedRoutes.push({ path: '*', redirect: '/404', hidden: true })
                commit("UPDATE_ACCESSED_ROUTES", accessedRoutes)
               resolve(accessedRoutes)
            })
        }

    },
    getters: {
        sidBar: state => state.sidBar,
        leftsidBar: state => state.leftsidBar,
        accessedRoutes: state => state.accessedRoutes,
        openRoutes: state => state.openRoutes,
        openRouteNames: state => state.openRouteNames,
    },
    modules: {
    }
};

export default route;