import {StorageUtil} from "@/utils/StorageUtil";
import DataUtils from "@/utils/DataUtils";
import {filterAsyncRouter} from "@/utils/routeHook";

const Info = {
    state: {
        // 登录信息
        userInfo: StorageUtil.getItem('userInfo') || {},
        // 登录token
        token: "",
    },
    mutations: {
        UPDATE_USER_INFO(state, userInfo) {
            state.userInfo = userInfo.user || {};
            StorageUtil.setItem("userInfo", userInfo.user);
            state.token = userInfo.token || "";
            StorageUtil.setItem("TOKEN", userInfo.token)
        },
        CLEAR_USER_INFO(state) {
            state.userInfo = {};
            state.token = "";
            StorageUtil.clear();
        }
    },
    actions: {
        update_user_info({ commit, state }, userInfo) {
            commit("UPDATE_USER_INFO", userInfo);
        },
        clear_user_info({ commit, state }) {
            commit("CLEAR_USER_INFO");
        },
    },
    getters: {
        userInfo: state => state.userInfo,
    },

}
export default Info;