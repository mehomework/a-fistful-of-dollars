const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  runtimeCompiler: true,
  outputDir: "childhood-gateway",
  publicPath: "/childhood/gateway/",
  devServer: {
    proxy: {
      '/api*': {
        target: 'http://localhost:9908/wemedia',
        changeOrigin: true, // 跨域
        // pathRewrite : {
          // "^/wemedia" : ""
        // }
      },
    },
    port: 9090, // 端口
  }
})
