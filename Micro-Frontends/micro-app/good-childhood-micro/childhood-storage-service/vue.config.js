const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  runtimeCompiler: true,
  outputDir: "childhood-storage-service",
  publicPath: "/childhood/storage-service/",
  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    proxy: {
      '/api*': {
        target: 'http://localhost:9908/wemedia',
        changeOrigin: true, // 跨域
        // pathRewrite : {
        // "^/wemedia" : ""
        // }
      },
    },
    port: 9080, // 端口
  }
})
