import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// entry
import './public-path'

Vue.config.productionTip = false
// 监听卸载操作
window.addEventListener('unmount', function () {
  console.log("storage-service 卸载了！")
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#storage-service')
