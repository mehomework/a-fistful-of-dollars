const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  outputDir: 'vue2',
  publicPath: '/child/vue2/',
  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*',
    }
  }
})
