const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  outputDir: 'main-vue2',
  publicPath: '/main-vue2/',
  transpileDependencies: true
})
