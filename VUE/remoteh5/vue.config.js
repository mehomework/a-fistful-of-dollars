const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: "/remotebank/h5/",
  devServer: {
    proxy: {
      '/remotebank*': {
        target: 'https://rfs.rongdata-ibg.com:19080/remotebank',
        changeOrigin: true, // 跨域
        pathRewrite : {
          "^/remotebank" : ""
        }
      }
    },
    port: 9090, // 端口
  }
})
