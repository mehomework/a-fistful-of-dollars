import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    globalData: {
      socket: null,
      bankCode: '0322',
      sessionId: '',
      random: '',
      wsToken: '',
      roomNum: '',
      h5Type: '',
      meetingNum: '',
      reserveId: '',
      productCode: '',
      userToken: '',
      access_token: '',
      url: '',
      infor: {},
      showUrl: false,
      isNetConnected: true,
      isAndroid: false,
      isStopRecord: false
    }
  },
  getters: {
    globalData: state => state.globalData
  },
  mutations: {
    UPDATE_GLOBAL(state, data = {key: "", val: ""}) {
      state.globalData[data.key] = data.val;
    },
    REST_GLOBAL(state) {
      state.globalData = {
        socket: null,
        bankCode: '0322',
        sessionId: '',
        random: '',
        wsToken: '',
        roomNum: '',
        h5Type: '',
        meetingNum: '',
        reserveId: '',
        productCode: '',
        userToken: '',
        access_token: '',
        url: '',
        infor: {},
        showUrl: false,
        isNetConnected: true,
        isAndroid: false,
        isStopRecord: false
      }
    },
  },
  actions: {
  },
})
