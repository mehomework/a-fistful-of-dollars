import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import {getAPIs} from './apis/getAPIClass'
import Vant from 'vant';
import 'vant/lib/index.css';

//H5加载引入app集成
import "./utils/app/mbankVideoChat.js"

Vue.use(Vant);
Vue.config.productionTip = false

Vue.prototype.$getAPIs = getAPIs;

// 添加全局前置守卫
router.beforeEach((to, from, next) => {
  console.log(to, from)

  if (to.meta && to.meta.title) {
    document.title = to.meta.title || "渣打银行保险e通"
  }

  next();
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
