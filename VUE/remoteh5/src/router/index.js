import Vue from 'vue'
import VueRouter from 'vue-router'
import login from '../views/login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    meta: {
      title: "渣打银行保险e通"
    },
    component: login
  },
  {
    path: '/orderList',
    name: 'orderList',
    meta: {
      title: "预约记录"
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/orderList.vue')
  },
  {
    path: '/wechat-orc',
    name: 'wechatOrc',
    meta: {
      title: "上传证件"
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/wechatOrc/index.vue')
  },
  {
    path: '/videoRoom',
    name: 'videoRoom',
    meta: {
      title: "视频通话"
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/videoRoom.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
