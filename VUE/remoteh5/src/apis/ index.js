import serviceIo from '@/apis/request';
import urls from '@/apis/urls';

const post = {};
// 遍历urls文件中所有的模板
urls.forEach((module) => {
    post[module.moduleName] = {};
    console.log(module)
    module.urls.forEach(url => {
        post[module.moduleName][url.name] = data => serviceIo({
            url: url.url,
            method: url.type,
            data
        })
    })
})

export default post;