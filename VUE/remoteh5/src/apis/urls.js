export class PostUrl { constructor(name, url, type) { return { name, url, type } } }
export class PostModule { constructor(moduleName, baseUrl, urls) { return { moduleName, baseUrl, urls } } }

// 设置公共路径
const basePath = '/remotebank'
console.log(basePath);


const postModules = [
    new PostModule('apis', '', [
        // 获取token
        new PostUrl('seachToken', `${basePath}/api/user/rb/auth/token`, 'post'),
        // 登录
        new PostUrl('login', `${basePath}/api/user/rb/enterMeeting/enter`, 'post'),
        // 预约刷新
        new PostUrl('refOrderList', `${basePath}/api/user/rb/enterMeeting/refresh`, 'post'),
        // 上传证件并ocr
        new PostUrl('uploadeIdcard', `${basePath}/api/user/ocr/idCard`, 'post'),
    ]),
]
export default postModules;