import post from "@/apis/ index"
import { Toast } from "vant"
export const getAPIs = async (that, params, message, main, minor) => {
    try {
        that.loading = true;
        const resData = await post[main][minor](params);
        if (resData.status != 200) return Toast(resData.statusText);

        const config = resData.config;

        if (config.url.indexOf('/enterMeeting/enter') > -1) {
            that.$store.commit("UPDATE_GLOBAL", {key: "sessionId", val: resData.headers.sessionid || resData.headers.sessionId || ""})
        }
        const code = resData && resData.data && resData.data.status;
        if (code === 0 || code === 200) {
            return resData.data
        } else if (code === -1) {
            Toast(resData.data.msg || '接口请求失败');
        } else if (code === 500) {
            Toast(resData.data.msg || resData.data.error || '接口请求失败');
        }

        return false

    } catch (e) {
        console.error(e)
        return false
    } finally {
        that.loading = false;
    }
}