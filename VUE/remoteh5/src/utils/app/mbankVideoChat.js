
var mbankVideoChat = {
	
	/**
	 * 发起视频通话
	 * @param args json格式
	 * {
	 *   appID: 	    Zego 派发的数字 ID, 开发者的唯一标识
	 *  appSign:        Zego 派发的签名, 用来校验对应 appID 的合法性
	 *  userID:          用户 ID
	 * userName:        用户名
	 * videoDispatchUrl:音视频服务域名
	 * role：            角色
	 * channel：        房间号
	 * token：   鉴权token
	 * }
	 */
    startVideoChat: function(args) {
		mbankVideoChat.exec("startVideoChat", args);
    },
	
	loadURL: function(url) {
		var iFrame;
		iFrame = document.createElement("iframe");
		iFrame.setAttribute("src", url);
		iFrame.setAttribute("style", "display:none;");
		iFrame.setAttribute("height", "0px");
		iFrame.setAttribute("width", "0px");
		iFrame.setAttribute("frameborder", "0");
		document.body.appendChild(iFrame);
		iFrame.parentNode.removeChild(iFrame);
		iFrame = null;
	},

	exec: function(funName, args) {
		var commend = {
			functionName: funName,
			arguments: args
		};
		var jsonStr = JSON.stringify(commend);
		var url = "mbankvideochat:" + jsonStr;
		mbankVideoChat.loadURL(url);
	}
	
}

window.mbankVideoChat = mbankVideoChat;
